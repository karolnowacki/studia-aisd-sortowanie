#

CC = gcc

all: sortowanie.c
	$(CC) sortowanie.c -Wall -O2 -lm -o sortowanie

data:
	perl geninput.pl

plot:
	cd doc && gnuplot wykres_random.gnuplot && \
		gnuplot wykres_sorted.gnuplot && \
		gnuplot wykres_reversesorted.gnuplot



