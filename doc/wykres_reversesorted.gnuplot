set term png
set output "w3_reversesorted_0.png"

set grid
set title 'Reverse Sorted Numbers'
set xlabel 'N (problem size)'
set xtics rotate
set ylabel 'time [s]'

plot '../data/03_reversesorted.txt.timestats' u 1:2 w l t 'stdlib qsort', \
	'../data/03_reversesorted.txt.timestats' u 1:3 w l t 'quick sort', \
	'../data/03_reversesorted.txt.timestats' u 1:4 w l t 'buble sort', \
	'../data/03_reversesorted.txt.timestats' u 1:5 w l t 'insert sort', \
	'../data/03_reversesorted.txt.timestats' u 1:6 w l t 'selection sort', \
	'../data/03_reversesorted.txt.timestats' u 1:7 w l t 'merge sort', \
	'../data/03_reversesorted.txt.timestats' u 1:8 w l t 'heap sort'


set output "w3_reversesorted_1.png"

plot '../data/03_reversesorted.txt.timestats' u 1:2 w l t 'stdlib qsort', \
        '../data/03_reversesorted.txt.timestats' u 1:3 w l t 'quick sort', \
        '../data/03_reversesorted.txt.timestats' u 1:7 w l t 'merge sort', \
        '../data/03_reversesorted.txt.timestats' u 1:8 w l t 'heap sort'


set output "w3_reversesorted_2.png"
plot '../data/03_reversesorted.txt.timestats' u 1:4 w l t 'buble sort', \
        '../data/03_reversesorted.txt.timestats' u 1:5 w l t 'insert sort', \
        '../data/03_reversesorted.txt.timestats' u 1:6 w l t 'selection sort'
