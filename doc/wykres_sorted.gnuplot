set term png
set output "w2_sorted_0.png"

set grid
set title 'Sorted Numbers'
set xlabel 'N (problem size)'
set xtics rotate
set ylabel 'time [s]'

plot '../data/02_sorted.txt.timestats' u 1:2 w l t 'stdlib qsort', \
	'../data/02_sorted.txt.timestats' u 1:3 w l t 'quick sort', \
	'../data/02_sorted.txt.timestats' u 1:4 w l t 'buble sort', \
	'../data/02_sorted.txt.timestats' u 1:5 w l t 'insert sort', \
	'../data/02_sorted.txt.timestats' u 1:6 w l t 'selection sort', \
	'../data/02_sorted.txt.timestats' u 1:7 w l t 'merge sort', \
	'../data/02_sorted.txt.timestats' u 1:8 w l t 'heap sort'


set output "w2_sorted_1.png"

plot '../data/02_sorted.txt.timestats' u 1:2 w l t 'stdlib qsort', \
        '../data/02_sorted.txt.timestats' u 1:3 w l t 'quick sort', \
        '../data/02_sorted.txt.timestats' u 1:7 w l t 'merge sort', \
        '../data/02_sorted.txt.timestats' u 1:8 w l t 'heap sort'


set output "w2_sorted_2.png"
plot '../data/02_sorted.txt.timestats' u 1:4 w l t 'buble sort', \
        '../data/02_sorted.txt.timestats' u 1:5 w l t 'insert sort', \
        '../data/02_sorted.txt.timestats' u 1:6 w l t 'selection sort'
