set term png
set output "w1_random_0.png"

set grid
set title 'Random Numbers'
set xlabel 'N (problem size)'
set xtics rotate
set ylabel 'time [s]' 

plot '../data/01_random.txt.timestats' u 1:2 w l t 'stdlib qsort', \
	'../data/01_random.txt.timestats' u 1:3 w l t 'quick sort', \
	'../data/01_random.txt.timestats' u 1:4 w l t 'buble sort', \
	'../data/01_random.txt.timestats' u 1:5 w l t 'insert sort', \
	'../data/01_random.txt.timestats' u 1:6 w l t 'selection sort', \
	'../data/01_random.txt.timestats' u 1:7 w l t 'merge sort', \
	'../data/01_random.txt.timestats' u 1:8 w l t 'heap sort'


set output "w1_random_1.png"

plot '../data/01_random.txt.timestats' u 1:2 w l t 'stdlib qsort', \
        '../data/01_random.txt.timestats' u 1:3 w l t 'quick sort', \
        '../data/01_random.txt.timestats' u 1:7 w l t 'merge sort', \
        '../data/01_random.txt.timestats' u 1:8 w l t 'heap sort'


set output "w1_random_2.png"
plot '../data/01_random.txt.timestats' u 1:4 w l t 'buble sort', \
        '../data/01_random.txt.timestats' u 1:5 w l t 'insert sort', \
        '../data/01_random.txt.timestats' u 1:6 w l t 'selection sort'

