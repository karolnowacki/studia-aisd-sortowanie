#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <assert.h>

#include "const.h"

#define BUFF_CHUNK 8192

int is_file(const char* filename){
	struct stat buffer;
	if (stat(filename,&buffer) == 0)
		return !S_ISDIR(buffer.st_mode);
	return 0;
}

int check_sort(long long *tab, size_t n) {
	size_t i;
	for (i=1; i<n; ++i)
		if (tab[i-1] > tab[i]) {
#ifdef DEBUG
			printf("\ntab[%d] > tab[%d] => %Ld > %Ld\n", i-1, i, tab[i-1], tab[i]);
#endif
			return 0;
		}

	return 1;
}

char * build_path(char * path, const char * dir, const char * file, const char * addon) {
	int p = 0;
	path[p] = '\0';
	if (dir) {
		strncpy(path, dir, PATH_MAX);
		p = strlen(path);
		path[p++] = kPathSeparator;
		path[p] = '\0';
	}
	if (file) {
		strncpy(path+p, file, PATH_MAX-p);
		p = strlen(path);
	} else return path;
	if (addon) {
		strncpy(path+p, addon, PATH_MAX-p);
		p = strlen(path);
                path[p] = '\0';
	}
	return path;
}

inline long long llscanf(FILE *f, long long *a) {
	char c = 0;
	int sign = 1;
	while((c<'-'||c>'9'||c=='.'||c=='/') && c != EOF)
		c = fgetc(f);
	if (c == EOF) return EOF;

	*a = 0;
	if (c == '-') {
		sign = -1;
		c = fgetc(f);
	}

	while (c >= '0' && c <= '9') {
		*a = *a*10 + (c - '0');
		c = fgetc(f);
	}

	return *a *= sign;
}

static int llcmp(const void *p1, const void *p2) {
	return *(long long *)p1 - *(long long *)p2;
}

long long int diffTimeMicroSec(struct timeval s, struct timeval e) {
        return ((e.tv_sec - s.tv_sec)*1000000L + e.tv_usec) - s.tv_usec;
}

void std_qsort(long long *tab, size_t nmemb) {
	qsort(tab, nmemb, sizeof(*tab), llcmp);
}

void kjj_qsort(long long *tab, size_t nmemb) {
	long long pivot, *l, *r, *e, tmp;

	if (nmemb < 3) {
		if (nmemb > 1 && tab[0] > tab[1]) {
			tmp = tab[0];
			tab[0] = tab[1];
			tab[1] = tmp;
		}
		return;
	}

	l = tab;
	e = r = tab+(nmemb-1);

	pivot = (*l + *e + tab[nmemb/2])/3;

	while (1) {
		while (*l < pivot) ++l;
		while (*r > pivot) --r;

		if (l<r) {
			tmp = *l;
			*l = *r;
			*r = tmp;
			++l;
			--r;
		} else break;
	}

	kjj_qsort(tab, r-tab+1);
	kjj_qsort(r+1, nmemb-(r-tab)-1);
}

void kjj_bublesort(long long *tab, size_t nmemb) {
	int i,j;
	register long long temp;
	for (i=0; i<nmemb-1; ++i)
		for (j=0; j<nmemb-1-i; ++j)
			if (tab[j] > tab[j+1]) {
				temp = tab[j];
				tab[j] = tab[j+1];
				tab[j+1] = temp;
			}
}

void kjj_insertsort(long long *tab, size_t nmemb) {
	int i,j;
	long long x;
	for (i=1; i<nmemb; ++i) {
		x = tab[i];
		for (j=i-1; j>=0 && tab[j]>x; --j)
			tab[j+1] = tab[j];
		tab[j+1] = x;
	}
}

void kjj_selectsort(long long *tab, size_t nmemb) {
	int i,j,k;
	register long long temp;
	for (i=0; i<nmemb; ++i) {
		k=i;
		for (j=i+1; j<nmemb; ++j)
			if (tab[j]<tab[k])
				k=j;

		temp = tab[k];
		tab[k] = tab[i];
		tab[i] = temp;
	}
}


static long long int * _temp_kjj_mergesort = NULL;
static size_t _temp_kjj_mergesort_size = 0;

void kjj_mergesort(long long *tab, size_t nmemb) {
	size_t m;
	int i,j,k;
	if (nmemb>1) {
		m = nmemb/2;

		if (nmemb > _temp_kjj_mergesort_size) {
    	_temp_kjj_mergesort_size = nmemb;
      _temp_kjj_mergesort = realloc(_temp_kjj_mergesort, sizeof(*_temp_kjj_mergesort) * _temp_kjj_mergesort_size);
    }

		kjj_mergesort(tab, m);
		kjj_mergesort(tab+m, nmemb-m);

  	memcpy(_temp_kjj_mergesort, tab, nmemb*sizeof(*tab));

		for(i=k=0, j=m; i<m && j<nmemb;)
			if (_temp_kjj_mergesort[i] < _temp_kjj_mergesort[j])
				tab[k++] = _temp_kjj_mergesort[i++];
			else
				tab[k++] = _temp_kjj_mergesort[j++];
		while(i<m) tab[k++] = _temp_kjj_mergesort[i++];
		while(j<nmemb) tab[k++] = _temp_kjj_mergesort[j++];

	}
}


void kjj_heapify(long long *tab, size_t nmemb, int i) {
	long long m = i;
	long long l = 2*i + 1;
	long long r = 2*i + 2;
	long long temp;

	if (l < nmemb && tab[l] > tab[m])
		m = l;

	if (r < nmemb && tab[r] > tab[m])
		m = r;

	if (m != i) {
		temp = tab[i];
		tab[i] = tab[m];
		tab[m] = temp;

		kjj_heapify(tab, nmemb, m);
	}
}

void kjj_heapsort(long long *tab, size_t nmemb) {
	int i;
	long long temp;

	for (i=nmemb/2-1; i>=0; --i)
		kjj_heapify(tab, nmemb, i);

	for (i=nmemb-1; i>=0; --i) {
		temp = tab[0];
		tab[0] = tab[i];
		tab[i] = temp;
        	kjj_heapify(tab, i, 0);
	}
}


typedef void func_t(long long *,size_t);

/* Definicja stuktuty przypadku testowego */
typedef struct testcase testcase;
struct testcase {
        char * name;  // nazwa funkcji (używana w komunikatach)
        func_t * func;  // wskaźnik na funkcję pod całką
};

testcase tc[] = {
        { "stdlib qsort", std_qsort },
        { "quick sort", kjj_qsort },
        { "buble sort",  kjj_bublesort },
        { "insert sort", kjj_insertsort },
	{ "selection sort", kjj_selectsort },
	{ "merge sort", kjj_mergesort },
	{ "heap sort", kjj_heapsort },
};


int main(int argc, char *argv[]) {

	struct dirent *pDirent;
	DIR *pDir;
	long long int *tab, *testtab;
	char *name, path[PATH_MAX];
	FILE *fh;
	size_t len, mlen, step, tlen;
	struct timeval startTime, endTime;
	int tests = sizeof tc/sizeof *tc; // Liczba testów
	testcase *t;

	int i;

	if (argc < 2) {
		printf ("Usage: %s <dirname>\n", argv[0]);
		return 1;
	}

	pDir = opendir(argv[1]);
	if (pDir == NULL) {
		printf ("Cannot open directory '%s' (%s)\n", argv[1], strerror(errno));
		return 2;
	}
	printf("Opening dir: %s\n", argv[1]);
	while ((pDirent = readdir(pDir)) != NULL) {
		build_path(path, argv[1], pDirent->d_name, NULL);
		if (is_file(path)) {
			printf ("\n\nPlik: %s:\n", pDirent->d_name);
			if ((fh = fopen(path, "r")) == NULL) {
				printf("\tERROR: Cannot open file '%s' (%s)\n", path, strerror(errno));
				continue;
			}

			len = 0;
			if ((getline(&name, &len, fh)) < 0) {
				printf("\tERROR: Cannot read file (%s)\n", strerror(errno));
				fclose(fh);
				continue;
			}
			printf("\tName: %s\n", name);

			len = 0;
			mlen = BUFF_CHUNK;
			tab = malloc(mlen * sizeof(*tab));

			while (llscanf(fh, tab+(len++)) != EOF)
				if (len+2>mlen)
					tab = realloc(tab, (mlen+=BUFF_CHUNK) * sizeof(*tab));
			len--;
			fclose(fh);


			printf("\tNo. of elemements: %Ld\n", (long long)len);

			testtab = malloc(len*sizeof(*testtab));

			for (i=0; i<tests; ++i) {
		                t = tc+i;

				memcpy(testtab, tab, len*sizeof(*testtab));
				gettimeofday (&startTime, NULL);
				t->func(testtab, len);
				gettimeofday (&endTime, NULL);
				printf(" - %20s - %d elements - %Ld us\n", t->name, (int)len, diffTimeMicroSec(startTime, endTime));

				assert(check_sort(testtab, len) == 1);
			}

			if (len > 1000) {
				printf(" - Generowanie danych do wykresów...");

				build_path(path, argv[1], pDirent->d_name, ".timestats");
				if ((fh = fopen(path, "w")) == NULL) {
					printf("\tERROR: Cannot open file '%s' (%s)\n", path, strerror(errno));
					continue;
				}
				printf(" - [%s]\n", path);

				fprintf(fh, "# N");
				for (i=0; i<tests; ++i) {
					t = tc+i;
					fprintf(fh, ", %s", t->name);
				}

				step = len/20;

				for (tlen = step; tlen <= len; tlen += step) {
					printf("\r - Ciezka praca :) [%lu%%]", 5*tlen/step);
					fflush(stdout);
					fprintf(fh, "\n%d", (int)tlen);
					for (i=0; i<tests; ++i) {
						t = tc+i;

						memcpy(testtab, tab, tlen*sizeof(*testtab));
						gettimeofday (&startTime, NULL);
						t->func(testtab, tlen);
						gettimeofday (&endTime, NULL);
						fprintf(fh, ", %f", diffTimeMicroSec(startTime, endTime)/1000000.0);
					}
				}
				fprintf(fh, "\n");
				fclose(fh);
				printf("\nDone\n");
			}
			free(tab);
			free(testtab);
			free(name);
		}
	}
	closedir (pDir);

	return 0;
}
