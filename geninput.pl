#!/usr/bin/perl

use strict;
use warnings;

sub gen_file {
	my ($file, $name, @data) = @_;
	if (open(my $fh, ">", $file)) {
		print $fh $name, "\n";
		print $fh join(" ", @data);
		close $fh;
		return 1;
	}
	return 0;
}

gen_file("data/01_random.txt", "Dane losowe", 
	map { int(rand(10**6)) } (1 .. 50000));

gen_file("data/02_sorted.txt", "Dane posortowane",
	(1 .. 50000));

gen_file("data/03_reversesorted.txt", "Dane posortowane odwrotnie",
	reverse(1 .. 50000));



