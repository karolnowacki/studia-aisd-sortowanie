
#define BUFF_CHUNK 8192

const char kPathSeparator =
#ifdef _WIN32
                            '\\';
#else
                            '/';
#endif

#ifndef PATH_MAX 

#ifdef _WIN32

#ifdef _MAX_PATH
#define PATH_MAX _MAX_PATH
#else
#define PATH_MAX 4096
#endif

#else
#include <limits.h>
#endif

#endif
